import { default as React } from "react";
import { IState, Provider, state } from "../../../lib/state";
import { SafeStateComponent } from "@aicacia/safe_state_component-react";

export type IStateProviderProps = Record<string, unknown>;

export interface IStateProviderState {
  value: IState;
}

export class StateProvider extends SafeStateComponent<
  IStateProviderProps,
  IStateProviderState
> {
  private _isUpdating = false;
  state: IStateProviderState = {
    value: state.getState(),
  };

  onSetState = () => {
    if (!this._isUpdating) {
      this._isUpdating = true;
      process.nextTick(this.runSetState);
    }
  };

  runSetState = () => {
    this._isUpdating = false;
    this.safeSetState({ value: state.getState() });
  };

  componentDidMount() {
    super.componentDidMount();
    state.addListener("set-state", this.onSetState);
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    state.removeListener("set-state", this.onSetState);
  }

  render() {
    return <Provider value={this.state.value}>{this.props.children}</Provider>;
  }
}
