import { default as React } from "react";
import { StyleSheet } from "react-native";
import { Text, Modal, Portal, Surface } from "react-native-paper";
import { theme } from "../../../lib/theme";

export interface ISignInProps {
  visible: boolean;
  onDismiss(): void;
}

export class SignIn extends React.PureComponent<ISignInProps> {
  render() {
    return (
      <Portal>
        <Modal
          contentContainerStyle={styles.modal}
          visible={this.props.visible}
          onDismiss={this.props.onDismiss}
        >
          <Text>Sign In</Text>
        </Modal>
      </Portal>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    backgroundColor: theme.colors.surface,
    maxWidth: 480,
    padding: 16,
    display: "flex",
    marginLeft: "auto",
    marginRight: "auto",
  },
  content: {},
});
