import { default as React } from "react";
import { parse } from "url";
import { Router, Route } from "@aicacia/router-react";
import { IContext, UnhandledError } from "@aicacia/router";
import { Loading } from "../Loading";
import { location } from "../../../lib/location";
import { router } from "../../../lib/router";
import { Error404Page } from "../../pages/Error404/Error404Page";
import { Error500Page } from "../../pages/Error500/Error500Page";
import { HomePage } from "../../pages/Home/HomePage";
import { set, update, selectContext } from "../../../stores/lib/router";
import { connect } from "../../../lib/state";

const LoadingRoutes = () => <Loading />;

export interface IRoutesStateProps {
  context: IContext;
}
export interface IRoutesFunctionProps {}
export interface IRoutesImplProps
  extends IRoutesProps,
    IRoutesStateProps,
    IRoutesFunctionProps {}

export interface IRoutesProps {}

const RoutesConnect = connect<
  IRoutesStateProps,
  IRoutesFunctionProps,
  IRoutesProps
>(
  (state, _ownProps) => ({
    context: selectContext(state).toJS() as IContext,
  }),
  (_state, _ownProps, _stateProps) => ({})
);

class RoutesImpl extends React.PureComponent<IRoutesImplProps> {
  onChange = (context: IContext) => {
    set(context);
  };
  onFulfilled = (context: IContext) => {
    update(context);
    return context.url;
  };
  onRejected = (error: Error, context: IContext) => {
    if (error.message === UnhandledError.MESSAGE) {
      return Promise.reject(parse(router.paths.Error404(), true));
    } else {
      context.error = error;
      return Promise.reject(parse(router.paths.Error500(), true));
    }
  };
  render() {
    return (
      <Router
        location={location}
        router={router}
        Loading={LoadingRoutes}
        onChange={this.onChange}
        onFulfilled={this.onFulfilled}
        onRejected={this.onRejected}
        context={this.props.context}
      >
        <Route
          path="/404"
          meta={{ name: "Error404" }}
          Component={Error404Page}
        />
        <Route
          path="/500"
          meta={{ name: "Error500" }}
          Component={Error500Page}
        />
        <Route meta={{ name: "Home" }} Component={HomePage} />
      </Router>
    );
  }
}

export const Routes = RoutesConnect(RoutesImpl);
