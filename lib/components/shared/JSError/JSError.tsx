import { default as React } from "react";
import { View } from "react-native";
import { Text, Headline, Subheading } from "react-native-paper";

const RE_NEWLINE = /[^\r\n]+/g;

export interface IJSErrorProps {
  error: Error;
}

export class JSError extends React.PureComponent<IJSErrorProps> {
  render() {
    const { error } = this.props;

    console.error(error);

    return (
      <>
        <Headline>{error.name}</Headline>
        <Subheading>{error.message}</Subheading>
        <View>
          {(error.stack || "").split(RE_NEWLINE).map((line, index) => (
            <Text key={index}>{line}</Text>
          ))}
        </View>
      </>
    );
  }
}
