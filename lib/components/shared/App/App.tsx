import { default as React } from "react";
import { Routes } from "../Routes";
import { StateProvider } from "../StateProvider";
import { Provider as PaperProvider } from "react-native-paper";
import { location } from "../../../lib/location";
import { theme } from "../../../lib/theme";

export class App extends React.PureComponent {
  componentDidMount() {
    location.init();
  }
  componentWillUnmount() {
    location.remove();
  }
  render() {
    return (
      <StateProvider>
        <PaperProvider theme={theme}>
          <Routes />
        </PaperProvider>
      </StateProvider>
    );
  }
}
