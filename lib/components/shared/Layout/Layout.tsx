import { default as React } from "react";
import { View, StyleSheet } from "react-native";
import { Appbar, Menu } from "react-native-paper";
import { SignIn } from "../SignIn";
import { theme } from "../../../lib/theme";

export interface ILayoutProps {
  children: NonNullable<React.ReactNode>;
}

export interface ILayoutState {
  menuOpened: boolean;
  signInOpened: boolean;
}

export class Layout extends React.PureComponent<ILayoutProps, ILayoutState> {
  state: ILayoutState = {
    menuOpened: false,
    signInOpened: false,
  };

  onMenu = () => this.setState({ menuOpened: !this.state.menuOpened });
  onSignIn = () =>
    this.setState({
      signInOpened: !this.state.signInOpened,
      menuOpened: false,
    });

  render() {
    return (
      <View style={styles.root}>
        <Appbar.Header>
          <Menu
            visible={this.state.menuOpened}
            onDismiss={this.onMenu}
            anchor={
              <Appbar.Action icon="dots-vertical" onPress={this.onMenu} />
            }
          >
            <Menu.Item onPress={this.onSignIn} title="Sign In" />
          </Menu>
        </Appbar.Header>
        <View style={styles.container}>{this.props.children}</View>
        <SignIn visible={this.state.signInOpened} onDismiss={this.onSignIn} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    overflow: "hidden",
    width: "100%",
    height: "100%",
    backgroundColor: theme.colors.background,
  },
  container: {
    maxWidth: 1280,
    paddingLeft: 16,
    paddingRight: 16,
    width: "100%",
    height: "100%",
    display: "flex",
    boxSizing: "border-box",
    marginLeft: "auto",
    marginRight: "auto",
  },
});
