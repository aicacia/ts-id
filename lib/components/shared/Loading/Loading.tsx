import { default as React } from "react";
import { StyleSheet, View } from "react-native";
import { ActivityIndicator } from "react-native-paper";
import { theme } from "../../../lib/theme";

export class Loading extends React.PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" animating={true} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: theme.colors.background,
  },
});
