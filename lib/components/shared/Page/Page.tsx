import { default as React } from "react";

export interface IPageProps {
  children: React.ReactNode;
}

export class Page extends React.PureComponent<IPageProps> {
  render() {
    return this.props.children;
  }
}
