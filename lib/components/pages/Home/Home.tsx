import { default as React } from "react";
import { Text } from "react-native-paper";
import { Page } from "../../shared/Page";
import { connect } from "../../../lib/state";
import { Layout } from "../../shared/Layout";

interface IHomeStateProps {}
interface IHomeFunctionProps {}
interface IHomeImplProps extends IHomeStateProps, IHomeFunctionProps {}

export interface IHomeProps {}
export interface IHomeState {}

const HomeConnect = connect<IHomeStateProps, IHomeFunctionProps, IHomeProps>(
  (state, _ownProps) => ({}),
  (_state, _ownProps, _stateProps) => ({})
);

class HomeImpl extends React.PureComponent<IHomeImplProps, IHomeState> {
  render() {
    return (
      <Page>
        <Layout>
          <Text>Hello, world!</Text>
        </Layout>
      </Page>
    );
  }
}

export const Home = HomeConnect(HomeImpl);
