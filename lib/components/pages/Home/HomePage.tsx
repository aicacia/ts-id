import { default as React } from "react";
import { Async } from "@aicacia/async_component-react";
import { JSError } from "../../shared/JSError";
import { Loading } from "../../shared/Loading";

export const HomePage = () => (
  <Async
    promise={import("./Home")}
    onSuccess={({ Home }) => <Home />}
    onError={(error) => <JSError error={error} />}
    onPending={() => <Loading />}
  />
);
