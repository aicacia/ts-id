import { default as React } from "react";
import { Page } from "../../shared/Page";
import { connect } from "../../../lib/state";
import { Layout } from "../../shared/Layout";
import { Headline, Paragraph } from "react-native-paper";

interface IError404StateProps {}
interface IError404FunctionProps {}
interface IError404ImplProps
  extends IError404StateProps,
    IError404FunctionProps {}

export interface IError404Props {}
export interface IError404State {}

const Error404Connect = connect<
  IError404StateProps,
  IError404FunctionProps,
  IError404ImplProps
>(
  (_state) => ({}),
  (_state, _ownProps, _stateProps) => ({})
);

class Error404Impl extends React.PureComponent<
  IError404ImplProps,
  IError404State
> {
  render() {
    return (
      <Page>
        <Layout>
          <Headline>404</Headline>
          <Paragraph>Not found</Paragraph>
        </Layout>
      </Page>
    );
  }
}

export const Error404 = Error404Connect(Error404Impl);
