import { default as React } from "react";
import { Async } from "@aicacia/async_component-react";
import { JSError } from "../../shared/JSError";
import { Loading } from "../../shared/Loading";

export const Error404Page = () => (
  <Async
    promise={import("./Error404")}
    onSuccess={({ Error404 }) => <Error404 />}
    onError={(error) => <JSError error={error} />}
    onPending={() => <Loading />}
  />
);
