import { default as React } from "react";
import { Async } from "@aicacia/async_component-react";
import { JSError } from "../../shared/JSError";
import { Loading } from "../../shared/Loading";

export const Error500Page = () => (
  <Async
    promise={import("./Error500")}
    onSuccess={({ Error500 }) => <Error500 />}
    onError={(error) => <JSError error={error} />}
    onPending={() => <Loading />}
  />
);
