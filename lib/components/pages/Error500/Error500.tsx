import { default as React } from "react";
import { Page } from "../../shared/Page";
import { connect } from "../../../lib/state";
import { Layout } from "../../shared/Layout";
import { Headline, Paragraph } from "react-native-paper";

export interface IError500StateProps {}
export interface IError500FunctionProps {}
export interface IError500ImplProps
  extends IError500Props,
    IError500StateProps,
    IError500FunctionProps {}

export interface IError500Props {}
export interface IError500State {}

const Error500Connect = connect<
  IError500StateProps,
  IError500FunctionProps,
  IError500Props
>(
  (_state, _ownProps) => ({}),
  (_state, _ownProps, _stateProps) => ({})
);

class Error500Impl extends React.PureComponent<
  IError500ImplProps,
  IError500State
> {
  constructor(props: IError500ImplProps) {
    super(props);

    this.state = {};
  }
  render() {
    return (
      <Page>
        <Layout>
          <Headline>500</Headline>
          <Paragraph>Internal Application Error</Paragraph>
        </Layout>
      </Page>
    );
  }
}

export const Error500 = Error500Connect(Error500Impl);
