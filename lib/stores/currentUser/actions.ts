import { none, some } from "@aicacia/core";
import { IJSON } from "@aicacia/json";
import AsyncStorage from "@react-native-community/async-storage";
import Axios, { AxiosResponse } from "axios";
import { List, Record } from "immutable";
import { state } from "../../lib/state";
import {
  Email,
  CurrentUser,
  ICurrentUser,
  ICurrentUserJSON,
  STORE_NAME,
  USER_TOKEN,
} from "./definitions";
import { isSignedIn } from "./selectors";

export const store = state.getStore(STORE_NAME);

function CurrentUserFromJSON(json: ICurrentUserJSON) {
  return CurrentUser({
    id: json.id,
    token: json.token,
    username: json.username,
    email: json.email ? some(Email(json.email)) : none(),
    emails: List(json.emails.map(Email)),
  });
}

store.fromJSON = (json: IJSON) => CurrentUserFromJSON(json as any);

export async function autoSignIn() {
  const token = await AsyncStorage.getItem(USER_TOKEN);

  if (token) {
    return signInWithToken(token);
  } else {
    return Promise.reject(new Error("Token not available"));
  }
}

export function signInWithToken(token: string) {
  if (!isSignedIn()) {
    return Axios.get<ICurrentUserJSON>("/user/current", {
      headers: {
        Authorization: token,
      },
    }).then(signInUser);
  } else {
    return store.getState();
  }
}

async function signInUser(response: AxiosResponse<ICurrentUserJSON>) {
  const currentUser = CurrentUserFromJSON(response.data),
    tokenKey = currentUser.get("token");

  Axios.defaults.headers.common.Authorization = tokenKey;
  await AsyncStorage.setItem(USER_TOKEN, tokenKey);

  store.updateState(() => currentUser);

  return currentUser;
}

export async function signOut(): Promise<Record<ICurrentUser>> {
  if (isSignedIn()) {
    const promise = Axios.delete("/user/current"),
      currentUser = CurrentUser();

    delete Axios.defaults.headers.common.Authorization;
    await AsyncStorage.removeItem(USER_TOKEN);

    store.updateState(() => currentUser);

    return promise.then(() => currentUser);
  } else {
    return store.getState();
  }
}
