import { none, Option } from "@aicacia/core";
import { List, Record } from "immutable";

export interface IEmail {
  email: string;
  primary: boolean;
  confirmed: boolean;
}

export type IEmailJSON = IEmail;

export const Email = Record<IEmail>({
  email: "",
  primary: false,
  confirmed: false,
});

export interface ICurrentUser {
  id: string;
  token: string;
  username: string;
  email: Option<Record<IEmail>>;
  emails: List<Record<IEmail>>;
}

export const CurrentUser = Record<ICurrentUser>({
  id: "",
  token: "",
  username: "",
  email: none(),
  emails: List(),
});

export interface ICurrentUserJSON {
  id: string;
  token: string;
  username: string;
  email?: IEmailJSON;
  emails: IEmailJSON[];
}

export const INITIAL_STATE = CurrentUser();
export const STORE_NAME = "currentUser";
export const USER_TOKEN = "X-Aicacia-Id-CurrentUser-Token";
