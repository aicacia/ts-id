import { fromJS, Map } from "immutable";
import { state } from "../../../lib/state";
import { IRouterContext, IRouterJSON, Router, STORE_NAME } from "./definitions";

export const store = state.getStore(STORE_NAME);

store.fromJSON = (json: IRouterJSON) =>
  Router({
    nextUrl: json.nextUrl ? fromJS(json.nextUrl) : undefined,
    url: fromJS(json.url),
    context: fromContext(json.context),
  });

export function fromContext(context: IRouterContext): Map<string, any> {
  return Map({
    ...context,
    params: Map(context.params),
    url: Map({
      ...context.url,
      query: Map(context.url.query),
    }),
  });
}

export function set(context: IRouterContext) {
  store.updateState((state) =>
    state.set("nextUrl", fromContext(context).get("url"))
  );
}

export function update(context: IRouterContext) {
  store.updateState((state) => {
    const nextUrl = state.get("nextUrl", undefined);
    return state.set("context", fromContext(context)).set("url", nextUrl);
  });
}
