import { default as React } from "react";
import { Map } from "immutable";
import { IState } from "../../../lib/state";
import { STORE_NAME } from "./definitions";

export const selectContext = (state: IState) =>
  state.get(STORE_NAME).get("context", Map());

export const selectComponent = (state: IState) =>
  selectContext(state).get("Component") as React.ComponentType | undefined;

export const selectUrlParam = (
  state: IState,
  param: string,
  defaultValue?: string
): string =>
  selectContext(state)
    .get("params", Map<string, string>())
    .get(param, defaultValue);

export const selectQueryParam = (
  state: IState,
  param: string,
  defaultValue?: number | string
): number | string =>
  selectContext(state)
    .get("url", Map())
    .get("query", Map())
    .get(param, defaultValue);

export const selectParam = (state: IState, param: string): string | number =>
  selectUrlParam(state, param, selectQueryParam(state, param) as any);
