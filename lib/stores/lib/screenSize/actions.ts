import { debounce } from "@aicacia/debounce";
import { Dimensions, Platform } from "react-native";
import { state } from "../../../lib/state";
import {
  IScreenSizeJSON,
  ScreenSize,
  STORE_NAME,
  TIMEOUT,
} from "./definitions";

export const store = state.getStore(STORE_NAME);

store.fromJSON = (json: IScreenSizeJSON) =>
  ScreenSize({
    width: json.width,
    height: json.height,
  });

export const sync = debounce((width: number, height: number) => {
  store.updateState((state) => state.set("width", width).set("height", height));
}, TIMEOUT);

if (Platform.OS == "web") {
  const onChange = () => {
    sync(window.innerWidth, window.innerHeight);
  };

  window.addEventListener("resize", onChange);
  window.addEventListener("orientationchange", onChange);
} else {
  Dimensions.addEventListener("change", ({ window }) =>
    sync(window.width, window.height)
  );
}
