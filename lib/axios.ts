import Axios from "axios";

Axios.defaults.baseURL = global.env.APP_API_ID_URL;
Axios.defaults.headers.common.Accept = "application/json";
Axios.defaults.headers.common["Content-Type"] = "application/json";
Axios.defaults.withCredentials = true;
