import packageJSON from "../package.json";

const env: NodeJS.Env = {
  ...process.env,
  ...((packageJSON.env as any)[process.env.NODE_ENV || "development"] || {}),
} as any;

global.env = Object.keys(env).reduce((globalEnv, key) => {
  if (isAppGlobalEnv(key)) {
    globalEnv[key] = env[key];
  }
  return globalEnv;
}, (global.env || {}) as Record<keyof NodeJS.Env, NodeJS.Env[keyof NodeJS.Env]>);

function isAppGlobalEnv(key: string): key is keyof NodeJS.Env {
  return key.startsWith("APP_");
}
