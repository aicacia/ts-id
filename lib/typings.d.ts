declare module NodeJS {
  interface Env {
    APP_API_ID_URL: string;
  }
  interface Global {
    env: Env;
  }
}
