export class NotFoundError extends Error {
  static MESSAGE = "404 - Not found";

  constructor() {
    super(NotFoundError.MESSAGE);
  }
}
