import { DarkTheme } from "react-native-paper";
import { Theme } from "react-native-paper/lib/typescript/src/types";

export const theme: Theme = {
  ...DarkTheme,
  dark: true,
  mode: "adaptive",
  roundness: 1,
  colors: {
    ...DarkTheme.colors,
  },
};
