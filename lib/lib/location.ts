import { NodeAsyncLocation } from "@aicacia/location";
import { parse } from "url";

export const location = new NodeAsyncLocation(parse("http://0.0.0.0/", true));
