import { default as React } from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { App } from "../components/shared/App";

export const mountApp = () => {
  const rootNode = document.getElementById("root") as Element;
  unmountComponentAtNode(rootNode);

  if (process.env.NODE_ENV !== "production") {
    console.log("Mounting App");
  }

  render(<App />, rootNode);
};
