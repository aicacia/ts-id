import { none, Option } from "@aicacia/core";
import { Layer, Router as AicaciaRouter, Route } from "@aicacia/router";

class Router extends AicaciaRouter {
  paths: { [key: string]: (...args: any[]) => string };

  constructor(path = "/", parent: Option<Layer> = none()) {
    super(path, parent);

    this.Router = Router;
    this.paths = {};
  }

  addPath(name: string, layer: Layer) {
    const root = this.getRoot() as Router;
    root.paths[name] = (...args: any[]) => layer.format(...args);
  }

  route(...args: any[]): Route {
    const route = super.route(...args),
      meta = route.getMeta();

    if (meta && meta.name) {
      this.addPath(meta.name, route);
    }
    return route;
  }
}

export const router = new Router();
