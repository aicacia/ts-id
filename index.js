import { registerRootComponent } from "expo";
import "./lib/axios";
import { App } from "./lib/components/shared/App";

registerRootComponent(App);
